package com.sunbeam.dao;

import java.sql.SQLException;

import com.sunbeam.pojo.Question;

public interface IQuestionDao {

	String addQuestion(Question question) throws SQLException;
	
	String editQuestion(String question_text, int question_serial_no, int question_id) throws SQLException;
	
	String deleteQuestion(int questionId) throws SQLException;
}
