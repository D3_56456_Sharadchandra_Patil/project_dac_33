package com.sunbeam.dao;

<<<<<<< HEAD
import static com.sunbeam.dbUtils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sunbeam.pojo.User;

public class UserDaoImpl implements IUserDao {

	private Connection cn;
	private PreparedStatement pst1;
	private PreparedStatement pst2;
	private PreparedStatement pst3;
	private PreparedStatement pst4;
	
	public UserDaoImpl() throws ClassNotFoundException, SQLException {
		// get cn
		cn = fetchConnection();
		pst1 = cn.prepareStatement("select * from users where email=? and password=?");
		pst2 = cn.prepareStatement("insert into users (email,password,mobile,name,gender,address,birth_date) values(?,?,?,?,?,?,?)");
		pst3 = cn.prepareStatement("update users set address = ?, mobile = ? where email = ?");
		pst4 = cn.prepareStatement("UPDATE users SET password=? WHERE email = ?  AND password= ?");
		//System.out.println("voter dao created...");
	}
	
	@Override
	public User signIn(String email, String password) throws SQLException {
		
		pst1.setString(1, email);
		pst1.setString(2, password);
		// exec method : execQuery
		try (ResultSet rst = pst1.executeQuery()) {
			if (rst.next())
				return new User(rst.getInt(1), email,  password, rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getDate(8));
		}
		return null;
	}
	
	@Override
	public String signUp(User user) throws SQLException {
		
		pst2.setString(1, user.getEmail());
		pst2.setString(2, user.getPassword());
		pst2.setString(3, user.getMobile());
		pst2.setString(4, user.getName());
		pst2.setString(5, user.getGender());
		pst2.setString(6, user.getAddress());
		pst2.setDate(7, user.getBirth_date());
		
		int updatedRows = pst2.executeUpdate();
		if(updatedRows == 1)
		{
			return "User SignUp Successfully...";
		}
		else 
			return null;

	}
	
	@Override
	public String editProfile(String address, String mobile, String email) throws SQLException {
		
		pst3.setString(1, address);
		pst3.setString(2, mobile);
		pst3.setString(3, email);
		
		int updatedRows = pst3.executeUpdate();
		if(updatedRows == 1)
		{
			return "Profile Edited Successfully...";
		}
		else 
			return null;

	}

	@Override
	public String updatePassword(String email, String password, String newPassword) throws SQLException 
	{
		pst4.setString(1, newPassword);
		pst4.setString(2, email);
		pst4.setString(3, password);
		int updatePassword = pst4.executeUpdate();
			if(updatePassword == 1)
				return "Password updated successfully !!\n";
			return null;
	}
	
	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (pst4 != null)
			pst4.close();
		if (cn != null)
			cn.close();
		System.out.println("User dao cleaned up...");
	}

	
	

=======
public class UserDaoImpl {
>>>>>>> 498469c3d2474126ceb1030ace71d18ff98ba820

}
