package com.sunbeam.dao;

import static com.sunbeam.dbUtils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sunbeam.pojo.Poll;

public class PollDaoImpl implements IPollDao {

	private Connection cn;
	private PreparedStatement pst1;
	private PreparedStatement pst2;
	private PreparedStatement pst3;
	private PreparedStatement pst4;
	private PreparedStatement pst5;
	
	public PollDaoImpl() throws ClassNotFoundException, SQLException {
		// get cn
		cn = fetchConnection();
		pst1 = cn.prepareStatement("select * from polls where created_by = ?");
		pst2 = cn.prepareStatement("insert into polls(title,start_datetime,end_datetime,created_by) values(?,?,?,?)");
        pst3 = cn.prepareStatement("update polls set title = ?,start_datetime = ?,end_datetime = ? where id = ? and created_by = ?");
        pst4 = cn.prepareStatement("select id,title from polls between start_datetime = ? and end_datetime = ?");
        pst5 = cn.prepareStatement("select id from users where email = ?");
	}
	
	@Override
	public List<Poll> listPolls(String email) throws SQLException {
		List<Poll> polls = new ArrayList<>();
		int userId = 0;
		pst5.setString(1,email);
		ResultSet rst1 = pst5.executeQuery();
		if(rst1.next())
			userId = rst1.getInt(1);
		
		pst1.setInt(1,userId);
		try (ResultSet rst = pst1.executeQuery()) {
			if (rst.next())
				polls.add(new Poll(rst.getInt(1), rst.getString(2), rst.getDate(3), rst.getDate(4),rst.getInt(5)));	
		}
		return polls;
	}

	@Override
	public String createNewPoll(Poll poll) throws SQLException {
		pst2.setString(1, poll.getTitle());
		pst2.setDate(2, poll.getStart_datetime());
		pst2.setDate(3, poll.getEnd_datetime());
		pst2.setInt(4, poll.getCreated_by());
		
		int updatedRows = pst2.executeUpdate();
		if(updatedRows == 1)
		{
			return "Poll Created Successfully...";
		}
		else 
			return null;
	}
	
	@Override
	public String editPoll(String title, Date start_date, Date end_date, int pollId, int userId) throws SQLException {
		pst3.setString(1, title);
		pst3.setDate(2, start_date);
		pst3.setDate(3, end_date);
		pst3.setInt(4, pollId);
		pst3.setInt(5, userId);
		
		int updatedRows = pst3.executeUpdate();
		if(updatedRows == 1)
		{
			return "Poll Edited Successfully...";
		}
		else 
			return null;

	}
	
	@Override
	public List<Poll> listLivePolls(Date start_date, Date end_date) throws SQLException {
		List<Poll> polls = new ArrayList<>();
		pst4.setDate(1,start_date);
		pst4.setDate(2,end_date);
		try (ResultSet rst = pst4.executeQuery()) {
			if (rst.next())
				polls.add(new Poll(rst.getInt(1), rst.getString(2)));	
		}
		return polls;
	}
	
	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (cn != null)
			cn.close();
		System.out.println("Poll dao cleaned up...");
	}

	

	

}
