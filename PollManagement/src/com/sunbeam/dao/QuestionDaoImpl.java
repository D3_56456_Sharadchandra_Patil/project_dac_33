package com.sunbeam.dao;

import static com.sunbeam.dbUtils.DBUtils.fetchConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sunbeam.pojo.Question;

public class QuestionDaoImpl implements IQuestionDao {

	private Connection cn;
	private PreparedStatement pst1;
	private PreparedStatement pst2;
	private PreparedStatement pst3;
	
	public QuestionDaoImpl() throws ClassNotFoundException, SQLException {
		// get cn
		cn = fetchConnection();
		pst1 = cn.prepareStatement("insert into questions (question_text,option_A,option_B,option_C,option_D,poll_id,question_serial_no) values(?,?,?,?,?,?,?)");
		pst2 = cn.prepareStatement("update questions set question_text = ?, question_serial_no = ? where id = ?");
		pst3 = cn.prepareStatement("delete from questions where id = ?");
	}
	@Override
	public String addQuestion(Question question) throws SQLException {
		pst1.setString(1, question.getQuestion_text());
		pst1.setString(2, question.getOption_A());
		pst1.setString(3, question.getOption_B());
		pst1.setString(4, question.getOption_C());
		pst1.setString(5, question.getOption_D());
        pst1.setInt(6, question.getPoll_id());
        pst1.setInt(7, question.getQuestion_serial_no());
        
        int updatedRows = pst1.executeUpdate();
		if(updatedRows == 1)
		{
			return "Question Added Successfully...";
		}
		else 
			return null;
	}
	
	@Override
	public String editQuestion(String question_text, int question_serial_no, int question_id) throws SQLException {
		
		pst2.setString(1, question_text);
		pst2.setInt(2, question_serial_no);
		pst2.setInt(3, question_id);
		
		int updatedRows = pst2.executeUpdate();
		if(updatedRows == 1)
		{
			return "Question Edited Successfully...";
		}
		else 
			return null;
	}
	
	@Override
	public String deleteQuestion(int questionId) throws SQLException {
		pst3.setInt(1, questionId);
		
		int updatedRows = pst3.executeUpdate();
		if(updatedRows == 1)
		{
			return "Question Deleted Successfully...";
		}
		else 
			return null;
	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (cn != null)
			cn.close();
		System.out.println("Question dao cleaned up...");
	}
	
	
}
