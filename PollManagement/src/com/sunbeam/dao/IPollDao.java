package com.sunbeam.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import com.sunbeam.pojo.Poll;

public interface IPollDao {

	List<Poll> listPolls(String email) throws SQLException;
	
	String createNewPoll(Poll poll) throws SQLException;
	
	String editPoll(String title, Date start_date, Date end_date,int pollId, int userId) throws SQLException;
	
	List<Poll> listLivePolls(Date start_datetime, Date end_datetime) throws SQLException;
}
