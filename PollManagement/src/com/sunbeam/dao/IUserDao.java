package com.sunbeam.dao;

import java.sql.SQLException;

import com.sunbeam.pojo.User;

public interface IUserDao {

	User signIn(String email,String password) throws SQLException;
	
	String signUp(User user) throws SQLException;
	
	String editProfile(String address, String mobile, String email) throws SQLException;
	
	String updatePassword(String email, String password, String newPassword) throws SQLException ;
}
