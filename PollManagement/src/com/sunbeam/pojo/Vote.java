package com.sunbeam.pojo;

import java.time.LocalDateTime;

public class Vote {

	private int id;
	private int que_id;
	private int user_id;
	private String selected_option;
	private LocalDateTime answered_datetime;
	
	public Vote(int id, int que_id, int user_id, String selected_option, LocalDateTime answered_datetime) {
		super();
		this.id = id;
		this.que_id = que_id;
		this.user_id = user_id;
		this.selected_option = selected_option;
		this.answered_datetime = answered_datetime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQue_id() {
		return que_id;
	}

	public void setQue_id(int que_id) {
		this.que_id = que_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getSelected_option() {
		return selected_option;
	}

	public void setSelected_option(String selected_option) {
		this.selected_option = selected_option;
	}

	public LocalDateTime getAnswered_datetime() {
		return answered_datetime;
	}

	public void setAnswered_datetime(LocalDateTime answered_datetime) {
		this.answered_datetime = answered_datetime;
	}

	@Override
	public String toString() {
		return "Vote [id=" + id + ", que_id=" + que_id + ", user_id=" + user_id + ", selected_option=" + selected_option
				+ ", answered_datetime=" + answered_datetime + "]";
	}
	
	
}
