package com.sunbeam.pojo;


public class Question {

	private int id;
	private String question_text;
	private String option_A;
	private String option_B;
	private String option_C;
	private String option_D;
	private int poll_id;
	private int question_serial_no;
	
	public Question(int id, String question_text, String option_A, String option_B, String option_C, String option_D,
			int poll_id, int question_serial_no) {
		super();
		this.id = id;
		this.question_text = question_text;
		this.option_A = option_A;
		this.option_B = option_B;
		this.option_C = option_C;
		this.option_D = option_D;
		this.poll_id = poll_id;
		this.question_serial_no = question_serial_no;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestion_text() {
		return question_text;
	}

	public void setQuestion_text(String question_text) {
		this.question_text = question_text;
	}

	public String getOption_A() {
		return option_A;
	}

	public void setOption_A(String option_A) {
		this.option_A = option_A;
	}

	public String getOption_B() {
		return option_B;
	}

	public void setOption_B(String option_B) {
		this.option_B = option_B;
	}

	public String getOption_C() {
		return option_C;
	}

	public void setOption_C(String option_C) {
		this.option_C = option_C;
	}

	public String getOption_D() {
		return option_D;
	}

	public void setOption_D(String option_D) {
		this.option_D = option_D;
	}

	public int getPoll_id() {
		return poll_id;
	}

	public void setPoll_id(int poll_id) {
		this.poll_id = poll_id;
	}

	public int getQuestion_serial_no() {
		return question_serial_no;
	}

	public void setQuestion_serial_no(int question_serial_no) {
		this.question_serial_no = question_serial_no;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", question_text=" + question_text + ", option_A=" + option_A + ", option_B="
				+ option_B + ", option_C=" + option_C + ", option_D=" + option_D + ", poll_id=" + poll_id
				+ ", question_serial_no=" + question_serial_no + "]";
	}
	
	
}
