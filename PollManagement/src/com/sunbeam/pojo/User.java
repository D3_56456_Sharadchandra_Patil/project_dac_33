package com.sunbeam.pojo;

import java.sql.Date;

public class User {

	private int id;
	private String email;
	private String password;
	private String mobile;
	private String name;
	private String gender;
	private String address;
	private Date birth_date;
	
	public User(int id, String email, String password, String mobile, String name, String gender, String address,
			Date birth_date) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.mobile = mobile;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.birth_date = birth_date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", mobile=" + mobile + ", name=" + name + ", gender=" + gender
				+ ", address=" + address + ", birth_date=" + birth_date + "]";
	}

}
