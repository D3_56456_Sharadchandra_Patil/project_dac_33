package com.sunbeam.tester;

import java.util.Scanner;

import com.sunbeam.services.AccountServices;

public class Polling {

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {
			
						
			AccountServices accService = new AccountServices();
					
			System.out.println("*************************************************************************");
			System.out.println("\n#####################  Welcome to PollManagement Application #######################");
			System.out.println("\n## User Account services ## ");
			System.out.println("\n## User has to create account to avail services of UserPoll Management and Public Poll Sevices ## ");
			while (true) {
				try {

					System.out.println("\nPress one of the Menus to avail service : \n" 
							+ "  1. SignIn\r\n"
							+ "  2. SignUp\r\n" 
							+ "  3. Edit Profile\r\n"
							+ "  4. Change Password\r\n" 
							+ "  5. SignOut\r\n" 
							);
					System.out.print("\nYour choice : ");

					switch (sc.nextInt()) {
					case 1:
						accService.signIn();
						break;

					case 2:
						accService.signUp();
						break;

					case 3:
						accService.editProfile();
						break;

					case 4:
						accService.changePassword();
						break;
											
					case 5: 
						System.out.println("\n********* Thank you for using application ********** \n");
						System.exit(0);
						break;
					default:
						System.out.println("\nInvalid choice !!!");
						break;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}

